package explaination;

public class Demo {
	/*
	public static void main(String[] args) {
		Thread t = new Thread (()-> {
		});
	}
	*/
	public static void main(String[] args) throws InterruptedException {
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				System.out.println("We are now in thread "+Thread.currentThread().getName());//Thread-2
				System.out.println("current thread priority :" + Thread.currentThread().getPriority());
			}
			
		});
		
		t.setName("New Worker Thread");
		t.setPriority(Thread.MAX_PRIORITY);
		
		System.out.println("be4 starting a new thread, we are in thread "+Thread.currentThread().getName());
		//this will instruct the JVM to create a new thread 
		//and pass it to the operating system
		t.start();
		System.out.println("after starting a new thread, we are in thread "+Thread.currentThread().getName());
		
		Thread.sleep(10000);
	
	}
}
