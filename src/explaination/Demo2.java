package explaination;

public class Demo2 {
	public static void main(String[] args) throws InterruptedException {
		Thread t = new NewThread();
		t.start();
	}
	
	private static class NewThread extends Thread{
		@Override
		public void run() {
//			System.out.println("Hello from thread "+Thread.currentThread().getName());
			System.out.println("Hello from thread "+this.getName());
			
		}
	}
	
}
