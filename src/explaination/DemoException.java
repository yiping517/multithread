package explaination;

public class DemoException {

	public static void main(String[] args) {
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				throw new RuntimeException("Intentional Exception");
			}
			
		});
		t.setName("Misbehaving thread");
		
		t.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			
			@Override
			public void uncaughtException(Thread thread, Throwable e) {
				System.out.println("a critical error happened in thread "
						+" the error is " + e.getMessage());
			}
		});
		
		t.start();
	}

}
