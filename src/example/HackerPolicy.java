package example;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HackerPolicy {

	public static final int MAX_PASSWORD = 9999;
	public static void main(String[] args) {
		Random r = new Random();
		Vault v = new Vault(r.nextInt(MAX_PASSWORD));
		
		List<Thread> threads = new ArrayList<>();
		threads.add(new AscendingHackerThread(v));
		threads.add(new DescendingHackerThread(v));
		threads.add(new PolicyThread());
		
		for(Thread t : threads) {
			t.start();
		}
	}

	private static class Vault{
		private int password;
		
		public Vault(int password) {
			this.password = password;
		}
		
		public boolean isCorrectPassword(int guess) {
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return this.password == guess;
		}
	}
	
	private static abstract class HackerThread extends Thread{
		protected Vault vault;
		
		public HackerThread(Vault v) {
			this.vault = v;
			this.setName(this.getClass().getSimpleName());
			this.setPriority(Thread.MAX_PRIORITY);
		}
		
		@Override
		public void start() {
			System.out.println("starting thread : "+this.getName());
			super.start();
		}
	}
	
	private static class AscendingHackerThread extends HackerThread{
		
		public AscendingHackerThread(Vault v) {
			super(v);
		}
		
		@Override
		public void run() {
			for(int guess=0; guess<MAX_PASSWORD; guess++) {
				if(vault.isCorrectPassword(guess)) {
					System.out.println(this.getName() + " guessed the pwd "+guess);
					//stop the program
					System.exit(0);
				}
			}
		}
	}
	
	private static class DescendingHackerThread extends HackerThread{

		public DescendingHackerThread(Vault v) {
			super(v);
		}
		
		@Override
		public void run() {
			for(int guess = MAX_PASSWORD; guess>=0; guess--) {
				if(vault.isCorrectPassword(guess)) {
					System.out.println(this.getName() + " guessed the pwd "+guess);
					System.exit(0);
				}
			}
		}
	}
	
	private static class PolicyThread extends Thread{
		@Override
		public void run() {
			for(int i=10; i>0; i--) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println(i);
			}
			System.out.println("game over for u hackers");
			System.exit(0);
		}
	}
}
